#!/usr/bin/env python3
"""
Create an un-signed revocation document

... and remember kids, never roll your own crypto ;)
"""

import os
import requests

fmt_document = '''Version: 10
Type: Revocation
Currency: %s
Issuer: %s
IdtyUniqueID: %s
IdtyTimestamp: %s
IdtySignature: %s
'''

def getdoc(currency, issuer, i_uid, i_ts, i_sig):
    return fmt_document % (currency, issuer, i_uid, i_ts, i_sig)

if __name__ == '__main__':

    print('\nI will be requesting BMA results from a duniter node.')

    currency = os.environ.get('DUNITER_CURRENCY', 'g1-test')

    node_uri = os.environ.get('DUNITER_BMA', None)
    default = 'http://127.0.0.1:20900'
    if not node_uri:
        print('\nEnter a duniter-bma URI (default: %s), then hit <enter>' 
            % default)
        node_uri = input()
        if not node_uri: node_uri = default

    print('\nPaste the base58 pubkey as Issuer, then hit <enter>')
    pubkey = input()

    print('\nRequesting %s/wot/lookup/%s...' % (node_uri, pubkey))
    response = requests.get(node_uri + '/wot/lookup/%s' % pubkey)
    json = response.json()

    if 'results' in json:
        print('...results found.')
        its = json['results'][0]['uids'][0]['meta']['timestamp']
        iuid = json['results'][0]['uids'][0]['uid']
        isig = json['results'][0]['uids'][0]['self']
    else:
        print('...no results found, cannot continue!')
        exit(1)

    print('\n\n%s' % getdoc(currency, pubkey, iuid, its, isig))
