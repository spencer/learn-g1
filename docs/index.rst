========================================
`git.duniter.org/spencer/learn-g1/docs`_
========================================

.. _`git.duniter.org/spencer/learn-g1/docs`: https://git.duniter.org/spencer/learn-g1/-/blob/master/docs/index.rst

-------------------
Welcome to learn-g1
-------------------
This repository is intended for those who wish to learn how the G1 libre currency blockchain works under the hood.  It is helpful but unnecessary to have basic understanding of blockchains and python code.  Your geeky curiosity is enough.  Code samples in this repository are easy-to-read and require few libraries; they are meant for teaching, not for production.  This repository favors minimal documentation and RTFCode over RTFM.

-----------------
Table of Contents
-----------------

Getting Started:
  * requirements_: Preparing your environment to use this repository.
  * cryptography_basics_: Cryptography concepts to understand.
  * blockchain_basics_: Blockchain concepts to understand.

Secret Credentials -- Private and Public Keys:
  * generate_keys_: Users authenticate with secret credentials creating a pair of keys.

DUBP -- Duniter Blockchain Protocol Documents:
  * unsigned_transaction_: Users transfer G1 value via Transaction.
  * unsigned_identity_: Living beings bind their primary Public-Key to an Identity.
  * unsigned_membership_: Living beings adhere to the G1 licence via Membership.
  * unsigned_certification_: Members vouch for others via Certification.
  * unsigned_revocation_: Members may permanently retire their Identity and Membership via Revocation.
  * publish_dubp_document_: Signed DUBP Documents are published to a duniter node.
  * blockentry_to_document_: DUBP Documents are forged into Blocks in a compact-form.
  * merge_unsigned_txs_: Transactions may be constructed interactively.

The Web of Trust (a directed graph of member certifications):
  * jsonize_wot_: create a local WoT.json for later inspection.
  * wot_distances_: walk backwards from a member through the WoT.

Fundamental Helpers:
  * convert_encoding_: Information is encoded in a number of ways.
  * hash_message_: Hashes are used to precisely identify other information.
  * sign_message_: Information is signed by a user so that all may verify.
  * verify_signed_message_: Anyone in public can verify the signer of information.

Related and Useful -- but NOT part of the DUniter Blockchain Protocol
  * `encrypt_plaintext / decrypt_ciphertext`_: Encryption using only the recipient keys.
  * `pkencrypt_plaintext / pkdecrypt_ciphertext`_: Encryption using both sender and recipient keys.

---------------------
I value your feedback
---------------------

If you've found this useful, please consider making a G1 donation to the "learn-g1" public-key: *3A9A6RNzZ7fNBsTmRwhJWCnKmWW13DkQ41M2MULZPpjC* as a signal for me to continue further on this journey... or not.

.. image:: ../media/learn-g1-pubkey.png

If you find glaring errors in code or documentation, know that I regret if it wasted your time; I highly value my own time and respectfully value yours.  Please do not hesitate to share feedback via cesium+ message to the learn-g1 pubkey above, so I can correct mistakes and express my appreciation for your feedback with more than just "cheap talk".

Respectfully,

*-Spencer971*

.. _blockchain_basics: blockchain_basics.rst
.. _blockentry_to_document: blockentry_to_document.rst
.. _convert_encoding: convert_encoding.rst
.. _cryptography_basics: cryptography_basics.rst
.. _`encrypt_plaintext / decrypt_ciphertext`: encrypt_plaintext_decrypt_ciphertext.rst
.. _generate_keys: generate_keys.rst
.. _hash_message: hash_message.rst
.. _jsonize_wot: jsonize_wot.rst
.. _merge_unsigned_txs: merge_unsigned_txs.rst
.. _`pkencrypt_plaintext / pkdecrypt_ciphertext`: pkencrypt_plaintext_pkdecrypt_ciphertext.rst
.. _publish_dubp_document: publish_dubp_document.rst
.. _requirements: requirements.rst
.. _sign_message: sign_message.rst
.. _unsigned_certification: unsigned_certification.rst
.. _unsigned_identity: unsigned_identity.rst
.. _unsigned_membership: unsigned_membership.rst
.. _unsigned_revocation: unsigned_revocation.rst
.. _unsigned_transaction: unsigned_transaction.rst
.. _verify_signed_message: verify_signed_message.rst
.. _wot_distances: wot_distances.rst
