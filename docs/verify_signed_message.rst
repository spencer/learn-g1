=========================
verify_signed_message.py_
=========================

.. _verify_signed_message.py: ../verify_signed_message.py

Throughout the G1 blockchain and network, signatures are verified each-step-of-the-way in order for trustless peers to authenticate that the issuer did indeed sign precisely the information being claimed to have been signed.

In general, the entire DUBP Document was signed, from top to bottom inclusive of its last newline.

example from block #376477)

::
	
	$ ./verify_signed_message.py 
	
	Paste the signer's base58 public-key below, then hit <enter>
	3A9A6RNzZ7fNBsTmRwhJWCnKmWW13DkQ41M2MULZPpjC
	
	Paste the base64 signature of the message below, then hit <enter>
	K7fskEnThQK8X0mnHKiyBs1SjGBUl1SgtmsSSHG8/rLIrSUNUd48rC6h5d/BN+rj9NhWkZSeR/PQXgjkyQAtCQ==
	
	Paste the message below, then hit <Ctrl-D>
	Version: 10
	Type: Transaction
	Currency: g1
	Blockstamp: 376466-0000000A906E99D3D1661C847FDECA779FEEFB01C0DE74172D4E102CFE20445D
	Locktime: 0
	Issuers:
	3A9A6RNzZ7fNBsTmRwhJWCnKmWW13DkQ41M2MULZPpjC
	Inputs:
	5115:0:T:A956972023A98799E18816FB76AF6C897BEFA29E708B3672A29878AE31C6EB2D:0
	Unlocks:
	0:SIG(0)
	Outputs:
	2558:0:SIG(EVfy1VoZwbuN7L69kYiHxeosJLh5azkHV8G6TaSLy94r)
	2557:0:SIG(8txjWNFZhMJbKPijvnFybeksN1QpKaKJrM4jW8HhnFsX)
	Comment: Welcome Alice and Bob to LibreMoney!
	
	Message Signature Verified for 3A9A6RNzZ7fNBsTmRwhJWCnKmWW13DkQ41M2MULZPpjC

However, regarding Blocks, which are forged by member-nodes to secure the blockchain, the signature represents only the last two complete lines of the Block: InnerHash and Nonce.  Because the InnerHash is a verifably compact identifier for the rest of the Block, all information in the Block has effectively been signed.

example from Block #1) having:
  * bottom signature: zcF6EZF+i+U4KWsNjXGXbNNRRwO0BFjKl4EVsmpyv1qB4xzT+O2PRdkYoi8rH7/XX9stAhF+M/4pAA8YhkXKCQ==

.. image:: ../media/verify_signed_messageA.png

.. image:: ../media/verify_signed_messageB.png

::
	
	$ ./verify_signed_message.py 
	
	Paste the signer's base58 public-key below, then hit <enter>
	2ny7YAdmzReQxAayyJZsyVYwYhVyax2thKcGknmQy5nQ
	
	Paste the base64 signature of the message below, then hit <enter>
	zcF6EZF+i+U4KWsNjXGXbNNRRwO0BFjKl4EVsmpyv1qB4xzT+O2PRdkYoi8rH7/XX9stAhF+M/4pAA8YhkXKCQ==
	
	Paste the message below, then hit <Ctrl-D>
	InnerHash: 64B858824682AB9D046F31DDACF7524FCD2501AEF269EE09E95EF2414DC9F7DB
	Nonce: 10100000025565
	
	Message Signature Verified for 2ny7YAdmzReQxAayyJZsyVYwYhVyax2thKcGknmQy5nQ
