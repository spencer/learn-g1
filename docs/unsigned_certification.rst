==========================
unsigned_certification.py_
==========================

.. _unsigned_certification.py: ../unsigned_certification.py

A Certification document is created, signed, and published by an existing member when they want to vouch for another member, or for a future member.

This example was not published; Alice and Bob are fictitious characters)

::
	
	$ ./unsigned_certification.py 
	
	I will be requesting BMA results from a duniter node.
	
	Paste your base58 pubkey as Issuer, then hit <enter>
	8txjWNFZhMJbKPijvnFybeksN1QpKaKJrM4jW8HhnFsX
	
	Paste their base58 pubkey as IdtyIssuer, then hit <enter>
	EVfy1VoZwbuN7L69kYiHxeosJLh5azkHV8G6TaSLy94r
	
	Requesting http://localhost:20900/wot/lookup/EVfy1VoZwbuN7L69kYiHxeosJLh5azkHV8G6TaSLy94r...
	...results found.
	
	Requesting http://localhost:20900/blockchain/current...
	...current height is 377269.
	
	Requesting http://localhost:20900/blochain/block/377263...
	...using 377263-000000054767938E2D1C4A15A1A4701F8F3A68EA13210C58BFE8CA62220AAB0D.
	
	
	Version: 10
	Type: Certification
	Currency: g1
	Issuer: 8txjWNFZhMJbKPijvnFybeksN1QpKaKJrM4jW8HhnFsX
	IdtyIssuer: EVfy1VoZwbuN7L69kYiHxeosJLh5azkHV8G6TaSLy94r
	IdtyUniqueID: Alice
	IdtyTimestamp: 377247-0000000C6395EDA1E46B80891A59DE4F8D204BDE88435BC03ACECAF9C780DB43
	IdtySignature: W9BCAt8RYhEd7qy61nocR2EGZFl8fPsj/HVbOH/4QRwadgtaV7IWmEB7B2d3ut6bEsQYJj8kFXtoJhIOzO0FCg==
	CertTimestamp: 377263-000000054767938E2D1C4A15A1A4701F8F3A68EA13210C58BFE8CA62220AAB0D
	

