===================================================
Setup: Preparing your computing environment
===================================================

For those who are just reading the contents of this repository, none of the following will be necessary.  However, if you will be tinkering with code and/or running the code on your machine (be careful -- this is not production quality code), then this document may be helpful in preparing your computing environment.

-----------------------------------------
Install a Cesium wallet browser extension
-----------------------------------------
Cesium is available for Iphone, Android, desktops, and via extensions for Brave, Firefox and Chrome browsers.  The browser extension works great, allows to save your keys as an encrypted ewif on your computer, and serves as a convenient blockchain explorer.  Search-for and install Cesium within the extension-settings of your favorite browser.

---------------------
Python3, git and pip3
---------------------
The following examples will install "python3", "git", and "pip3" onto your local machine.

::
	
	$ sudo apt-get install python3  # intepretted programming language
	$ sudo apt-get install git      # source-code version control software
	$ sudo apt install python3-pip  # python3 package manager
	

-----------------------------------
External Python Packages
-----------------------------------

Effort was made to avoid requiring many external dependencies so that learning about G1 libre currency could be done without also learning many other 3rd-party libraries. Still, a few external python packages are required.
::
	
	$ pip3 install libnacl     # cryptography library
	$ pip3 install base58      # base58 encoding
	$ pip3 install termcolor   # for white on red "Warning!"
	$ pip3 install pyaes	   # encryption library
	$ pip3 install requests	   # http/https requests and responses
	

---------------------
Environment Variables
---------------------

*DUNITER_BMA* 

Some scripts will require a connection to an available duniter node.  You can set and export your DUNITER_BMA environment variable to a value that matches the format: "method://hostname-or-ip:bma-port" or do so in your ~/.bashrc file.

For Cesium users, find a list of duniter nodes by going into "Settings".  Under "Network" click "Duniter peer address" then "PEER'S LIST".


*DUNITER_CURRENCY*

Scripts that create DUBP documents will default to Currency=g1-test, the test network.  This can be changed via your DUNITER_CURRENCY environment varaible, to 'g1', the production network... but if you do so, be careful!

2 examples of setting your DUNITER_NODE and DUNITER_CURRENCY environment variables)

::
	
	$ export DUNITER_BMA=http://localhost:20900  # your own local node
	$ export DUNITER_BMA=https://g1.duniter.org  # a publicly available node
	
	$ export DUNITER_CURRENCY=g1-test            # default: test network
	$ export DUNITER_CURRENCY=g1                 # g1 production network
	
-----------------------
Cloning This Repository
-----------------------

Use git to clone this repository.

The following example will clone this repository, create a 'learn-g1' folder under your current working directory, and change directory to your own local copy.

::
	
	$ git clone https://git.duniter.org/spencer/learn-g1.git
	$ cd learn-g1


