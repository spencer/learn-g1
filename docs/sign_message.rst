================
sign_message.py_
================

.. _sign_message.py: ../sign_message.py

When users of libre currency want to interract with the blockchain, their software will request their secret credentials in order to sign the DUBP document. The client software will then append their signature to the bottom of the document before publishing to a node so that all may verify.

example signing of Transaction document, was later processed in Block #376477)

::
	
	$ ./sign_message.py 
	
	Warning!
	Paste your base58 private-key or seed, then hit <enter>
	For-security-reasons:base58-seed-has-been-hidden-from-this-documentation
	
	Paste the message below, then hit <Ctrl-D>
	Version: 10
	Type: Transaction
	Currency: g1
	Blockstamp: 376466-0000000A906E99D3D1661C847FDECA779FEEFB01C0DE74172D4E102CFE20445D
	Locktime: 0
	Issuers:
	3A9A6RNzZ7fNBsTmRwhJWCnKmWW13DkQ41M2MULZPpjC
	Inputs:
	5115:0:T:A956972023A98799E18816FB76AF6C897BEFA29E708B3672A29878AE31C6EB2D:0
	Unlocks:
	0:SIG(0)
	Outputs:
	2558:0:SIG(EVfy1VoZwbuN7L69kYiHxeosJLh5azkHV8G6TaSLy94r)
	2557:0:SIG(8txjWNFZhMJbKPijvnFybeksN1QpKaKJrM4jW8HhnFsX)
	Comment: Welcome Alice and Bob to LibreMoney!
	
	
	K7fskEnThQK8X0mnHKiyBs1SjGBUl1SgtmsSSHG8/rLIrSUNUd48rC6h5d/BN+rj9NhWkZSeR/PQXgjkyQAtCQ==
	The above signature for this message can be verified for pubkey 3A9A6RNzZ7fNBsTmRwhJWCnKmWW13DkQ41M2MULZPpjC
