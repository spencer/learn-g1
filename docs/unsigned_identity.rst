=====================
unsigned_identity.py_
=====================

.. _unsigned_identity.py: ../unsigned_identity.py

An Identity document is signed and published along with a Membership document in advance of membership to bind a simple-wallet's public key to a UniqueID (a unique pseudonym for a living person).

The following example was not published to the main network because Alice is a fictitious character)

::
	
	$ ./unsigned_identity.py 
	
	I will be requesting BMA results from a duniter node.
	
	Paste the base58 pubkey as Issuer, then hit <enter>
	EVfy1VoZwbuN7L69kYiHxeosJLh5azkHV8G6TaSLy94r
	
	Requesting http://localhost:20900/wot/lookup/EVfy1VoZwbuN7L69kYiHxeosJLh5azkHV8G6TaSLy94r...
	...no results found.
	
	Requesting http://localhost:20900/blockchain/current...
	...current height is 377253.
	
	Requesting http://localhost:20900/blochain/block/377247...
	...using 377247-0000000C6395EDA1E46B80891A59DE4F8D204BDE88435BC03ACECAF9C780DB43.
	
	Enter the new pseudonym as UniqueID, then hit <enter>
	Alice
	
	
	Version: 10
	Type: Identity
	Currency: g1
	Issuer: EVfy1VoZwbuN7L69kYiHxeosJLh5azkHV8G6TaSLy94r
	UniqueID: Alice
	Timestamp: 377247-0000000C6395EDA1E46B80891A59DE4F8D204BDE88435BC03ACECAF9C780DB43
	
