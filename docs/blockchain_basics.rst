===============================
BLOCKCHAIN BASICS - Terminology
===============================

Following are my thoughts on useful concepts that we might strive to understand, in order to benefit every-day utility from open-source distributed blockchains.

**Blockchain:**
Blocks are to a blockchain as pages are to a ledger.  Each block is a set of state changes that can be used by the chosen software to build a database which holds the current state... after all changes (blocks) have been applied; as well, each block is verifiably chained to the previous block; the entire set of blocks is known as the blockchain.  

When a blockchain is distributed among many peers, it is a common understanding that each peer cannot trust eachother... each can only trust itself, so they come to a consensus via the rules implemented in their software, about how to process each new block.  Communicating amongst themselves, each requests the next block from others, verifies that the block is valid according to its own rules, and decides to accept the block and share it with others, or to deny the block and consider ignoring future communications from the misbehaving peer that sent it an invalid block.

When we decide to use a blockchain, we're deciding to use a specific suite of software and to participate as a peer in a large peer-to-peer network of others who have chosen similar software.  Being distributed, we may have trouble finding 
the first peer (because we have no centralized server to connect to), however, once we find a single peer, we learn of others, and quickly reconnect among the network of existing peers who implement the same consensus rules as we do.  In fact, they can ALL disappear... and if one day a single peer comes back online and makes itself known to other users, the peer-to-peer network can be re-established in a verifiable way that all peers can trust... as if there never was any problem.

Each user of a blockchain controls their own set(s) of cryptographic keys.  When they interact, they are really sending cryptographically signed messages of their interaction which can be verified by all, by each peer, so that all can trust (as long as they protect their private-keys) that their actions could ONLY have been performed by themself.
