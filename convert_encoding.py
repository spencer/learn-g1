#!/usr/bin/env python3
"""
Convert between common encodings: hex, HEX, base58, base64, bytes

...and remember kids, never roll your own crypto ;)
"""

import sys
from codecs import escape_decode
from binascii import hexlify, unhexlify
from base64 import b64encode, b64decode

from base58 import b58encode, b58decode


encodings = ['hex', 'HEX', 'base58', 'base64', 'ascii', 'utf-8', 'bytes']
encodings_str = ','.join([x for x in encodings])

def convert(in_, from_, to_):
    if from_ == 'hex': 
        if not in_ == in_.lower():
            raise TypeError('%s is not hex' % in_)
        input_bytes = unhexlify(in_)
    elif from_ == 'HEX': 
        if not in_ == in_.upper():
            raise TypeError('%s is not HEX' % in_)
        input_bytes = unhexlify(in_)
    elif from_ == 'base58': input_bytes = b58decode(in_)
    elif from_ == 'base64': input_bytes = b64decode(in_)
    elif from_ == 'ascii': input_bytes = bytes(in_, 'ascii')
    elif from_ == 'utf-8': input_bytes = bytes(in_, 'utf-8')
    elif from_ == 'bytes': 
        if not in_.startswith("b'") and in_.endswith("'"):
            raise TypeError('%s is not bytes' % in_)
        input_bytes = escape_decode(in_[2:-1])[0]
    else: raise Exception("Unexpected program error")

    if to_ == 'hex': out_ = hexlify(input_bytes).decode('utf-8')
    elif to_ == 'HEX': out_ = hexlify(input_bytes).upper().decode('utf-8')
    elif to_ == 'base58': out_ = b58encode(input_bytes).decode('utf-8')
    elif to_ == 'base64': out_ = b64encode(input_bytes).decode('utf-8')
    elif to_ == 'ascii': out_ = input_bytes.decode('ascii')
    elif to_ == 'utf-8': out_ = input_bytes.decode('utf-8')
    elif to_ == 'bytes': out_ = input_bytes
    else: raise Exception("Unexpected program error")

    return out_

if __name__ == '__main__':

    class InputError(Exception): pass

    print("\nEnter the input string below, then hit <enter>")
    in_str = input()

    print("\nEnter the input encoding (%s), then hit <enter>" % encodings_str)
    from_ = input()
    if from_ not in encodings: 
        raise InputError('NOT one of (%s)' % encodings_str)


    print("\nEnter the output encoding (%s), then hit <enter>" % encodings_str)
    to_ = input()
    if to_ not in encodings: 
        raise InputError('NOT one of (%s)' % encodings_str)

    output = convert(in_str, from_, to_)

    print("\n\nInput converted from %s to %s\n%s" % (from_, to_, output))
