#!/usr/bin/env python3
'''
warning: i will consume cpu and ram.  
    Will i halt without being interrupted?

break the following assumption by brute force assert:
   "The 1st 32 bytes of an ed25519 signing/private-key is its seed"

Wondering: perhaps im confused about specifications of this project, 
assuming that keys are based on libNacl (which has a particular 
implementation of ed25519 where signing key has seed as first 32bytes)
when really this project's keys are based on ed25519 which doesnt
specify other than to sha512(seed)/do-some-bit-flips when producing
the actual private key used during creation of a signature?

originally inspired by: https://forum.duniter.org/t/definir-un-format-securise-pour-les-trousseaux-de-cles-g1/6904/113?u=spencer
'''

import sys
from random import Random, SystemRandom
from hashlib import scrypt
from base58 import b58encode
from libnacl.sign import Signer

rng_seed = SystemRandom().randint(0, 2**255)
print('Seeding Random() with %d' % rng_seed)
rng = Random(rng_seed)

attempt, old_password, old_salt, old_seed = 0, None, None, None
while True:
    attempt += 1
    #password = rng.randint(0, 2**255).to_bytes(32, sys.byteorder)
    #salt = rng.randint(0, 2**255).to_bytes(32, sys.byteorder)
    #seed = scrypt(password, salt=salt, n=16384, r=8, p=1, dklen=32)
    seed = rng.randint(0, 2**255).to_bytes(32, sys.byteorder)
    keypair = Signer(seed)
    try: 
        assert seed == keypair.seed == keypair.sk[:32]
    except AssertionError:
        print(' attempt: %d' % attempt)
        print('    seed: %s' % b58encode(seed).decode('utf-8'))
        print(' privkey: %s' % b58encode(keypair.sk).decode('utf-8'))
        print('  pubkey: %s' % b58encode(keypair.vk).decode('utf-8'))
        raise

    if attempt % 1000 == 0: 
        print('after %d attempts, privkey[:32] == seed' % attempt)

    # rng is not stuck
    #assert password != salt and password != old_password and salt != old_salt
    #old_password, old_salt = password, salt
    assert seed != old_seed
    old_seed = seed
