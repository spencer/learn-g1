#!/usr/bin/env python3
"""
Create an un-signed identity document

... and remember kids, never roll your own crypto ;)
"""

import os
import requests

fmt_document = '''Version: 10
Type: Identity
Currency: %s
Issuer: %s
UniqueID: %s
Timestamp: %s
'''

def getdoc(currency, issuer, uniqueid, timestamp):
    return fmt_document % (currency, issuer, uniqueid, timestamp)

if __name__ == '__main__':

    print('\nI will be requesting BMA results from a duniter node.')

    currency = os.environ.get('DUNITER_CURRENCY', 'g1-test')

    node_uri = os.environ.get('DUNITER_BMA', None)
    default = 'http://127.0.0.1:20900'
    if not node_uri:
        print('\nEnter a duniter-bma URI (default: %s), then hit <enter>' 
            % default)
        node_uri = input()
        if not node_uri: node_uri = default

    print('\nPaste the base58 pubkey as Issuer, then hit <enter>')
    pubkey = input()

    print('\nRequesting %s/wot/lookup/%s...' % (node_uri, pubkey))
    response = requests.get(node_uri + '/wot/lookup/%s' % pubkey)
    json = response.json()

    if 'results' in json:
        print('...results found.')
        ts = json['results'][0]['uids'][0]['meta']['timestamp']
        uid = json['results'][0]['uids'][0]['uid']
    else:
        print('...no results found.')

        print('\nRequesting %s/blockchain/current...' % node_uri)
        response = requests.get(node_uri + '/blockchain/current')
        json = response.json()
        latest = json['number']
        print('...current height is %d.' % latest)

        print('\nRequesting %s/blochain/block/%d...' % (node_uri, latest-6))
        response = requests.get(node_uri + '/blockchain/block/%d' % (latest-6))
        json = response.json()
        ts = '%d-%s' % (json['number'], json['hash'])
        print('...using %s.' % ts)

        print('\nEnter the new pseudonym as UniqueID, then hit <enter>')
        uid = input()

    print('\n\n' + getdoc(currency, pubkey, uid, ts))
