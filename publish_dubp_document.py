#!/usr/bin/env python3
"""
publish a signed DUBP document to a duniter node

... and remember kids, never roll your own crypto ;)
"""

import sys
import os

import requests 

document_types = ['Identity', 'Membership', 'Certification', 'Revocation',
    'Transaction']

def publish(signed_document, post_url, doc_varname):
    return requests.post(post_url, data={doc_varname: signed_document})

if __name__ == '__main__':

    class InputError(Exception): pass

    print("\nI will be posting to a duniter node.")

    node_uri = os.environ.get('DUNITER_BMA', None)
    default = 'http://127.0.0.1:20900'
    if not node_uri:
        print('\nEnter a duniter-bma URI (default: %s), then hit <enter>' 
            % default)
        node_uri = input()
        if not node_uri: node_uri = default

    print('\nEnter the DUBP Document type (%s), then hit <enter>' % ','.join(
        x for x in document_types))
    document_type = input()

    if document_type == 'Identity': 
        uri_suffix, doc_varname = '/wot/add', 'identity'
    elif document_type == 'Membership':
        uri_suffix, doc_varname = '/blockchain/membership', 'membership'
    elif document_type == 'Certification':
        uri_suffix, doc_varname = '/wot/certify', 'cert'
    elif document_type == 'Revocation':
        uri_suffix, doc_varname = '/wot/revoke', 'revocation'
    elif document_type == 'Transaction':
        uri_suffix, doc_varname = '/tx/process', 'transaction'
    else: raise Exception('Unexpected program error')

    print('\nPaste your signed %s document, then hit <Ctrl-D>' % document_type)
    signed_document = sys.stdin.read()

    print('\nPosting signed DUBP document as %s to %s%s...' % (
        doc_varname, node_uri, uri_suffix))
    response = publish(signed_document, ''.join([node_uri, uri_suffix]),
        doc_varname)

    print('\n\nresponse: %s' % response)
    print('response.json(): %s' % response.json())

