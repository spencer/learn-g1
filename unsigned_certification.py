#!/usr/bin/env python3
"""
Create an un-signed certification document

... and remember kids, never roll your own crypto ;)
"""

import os
import requests

fmt_document = '''Version: 10
Type: Certification
Currency: %s
Issuer: %s
IdtyIssuer: %s
IdtyUniqueID: %s
IdtyTimestamp: %s
IdtySignature: %s
CertTimestamp: %s
'''

def getdoc(currency, issuer, i_issuer, i_uid, i_ts, i_sig, cts):
    return fmt_document % (currency, issuer, i_issuer, i_uid, i_ts, i_sig, cts)

if __name__ == '__main__':

    print('\nI will be requesting BMA results from a duniter node.')

    currency = os.environ.get('DUNITER_CURRENCY', 'g1-test')

    node_uri = os.environ.get('DUNITER_BMA', None)
    default = 'http://127.0.0.1:20900'
    if not node_uri:
        print('\nEnter a duniter-bma URI (default: %s), then hit <enter>' 
            % default)
        node_uri = input()
        if not node_uri: node_uri = default

    print('\nPaste your base58 pubkey as Issuer, then hit <enter>')
    pubkey = input()

    print('\nPaste their base58 pubkey as IdtyIssuer, then hit <enter>')
    ipk = input()

    print('\nRequesting %s/wot/lookup/%s...' % (node_uri, ipk))
    response = requests.get(node_uri + '/wot/lookup/%s' % ipk)
    json = response.json()

    if 'results' in json:
        print('...results found.')
        its = json['results'][0]['uids'][0]['meta']['timestamp']
        iuid = json['results'][0]['uids'][0]['uid']
        isig = json['results'][0]['uids'][0]['self']
    else:
        print('...no results found, cannot continue!')
        exit(1)

    print('\nRequesting %s/blockchain/current...' % node_uri)
    response = requests.get(node_uri + '/blockchain/current')
    json = response.json()
    latest = json['number']
    print('...current height is %d.' % latest)

    print('\nRequesting %s/blochain/block/%d...' % (node_uri, latest-6))
    response = requests.get(node_uri + '/blockchain/block/%d' % (latest-6))
    json = response.json()
    ts = '%d-%s' % (json['number'], json['hash'])
    print('...using %s.' % ts)

    print('\n\n' + getdoc(currency, pubkey, ipk, iuid, its, isig, ts))
