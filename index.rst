=================================
`git.duniter.org/spencer/learn-g1`_
=================================

.. _`git.duniter.org/spencer/learn-g1`: https://git.duniter.org/spencer/learn-g1/-/blob/master/index.rst

.. image:: media/logo.png
   :scale: 50

Wanna take a ride?
------------------

Let's head-out on a journey towards **technical-understanding** of how the G1 
libre currency blockchain works.

Along the way, we'll re-invent the wheel a few times.  Will our wheels be any lighter, stronger, or better balanced than others?  It's not the point.  We're studying something interesting and useful, and to confirm it, we'll **implement** what has been learned.

I intend to **continuously challenge myself**, being prepared to share and effectively guide other coders and future coders along the way.  I'll also be studying existing tools and libraries in the G1 community.  Having re-invented wheels, we can **better appreciate** the projects of fellow developers.

We'll work towards the longer-term goal: **to offer measurable value by contributing** to new and existing community projects, **building on the shoulders of giants**, from a solid foundation and with **fresh perspective**.

So what do you say?  Join me and  `hop right in`_!

Your participation, help, and feedback is much appreciated.

*-Spencer971*

.. _`hop right in`: docs/index.rst
