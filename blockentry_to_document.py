#!/usr/bin/env python3
"""
Convert a compact block-entry to its original signed document, verify

...and remember kids, never roll your own crypto ;)
"""

import os
import sys
import requests

from verify_signed_message import verify
from unsigned_identity import getdoc as get_identity
from unsigned_membership import getdoc as get_membership
from unsigned_certification import getdoc as get_certification
from unsigned_revocation import getdoc as get_revocation
from unsigned_transaction import getdoc as get_transaction


def get_signed_identity(raw_entry, currency):
    pk, sig, _block, _uid = [x for x in raw_entry.split(':')]
    return pk, sig, get_identity(currency, pk, _uid, _block)

def get_signed_membership(raw_entry, currency, entry_type=None):
    in_ = 'OUT' if entry_type == 'leaver' else 'IN'
    pk, sig, _block, _block2, _uid = [x for x in raw_entry.split(':')]
    return pk, sig, get_membership(currency, pk, _block, in_, _uid, _block2)

def get_signed_certification(raw_entry, currency, node_uri):
    pk, _to, _block, sig = [x for x in raw_entry.split(':')]
    response = requests.get(node_uri + '/wot/lookup/%s' % _to)
    json = response.json()['results']
    _uid = json[0]['uids'][0]['uid']
    _its = json[0]['uids'][0]['meta']['timestamp']
    _isig = json[0]['uids'][0]['self']
    response = requests.get(node_uri + '/blockchain/block/%s' % _block)
    _cts = '%d-%s' % (int(_block), response.json()['hash'])
    if _block == '0':
        print('\nGENESIS EXCEPTION: using BlockUID from Identity for CertTimestamp')
        _cts = _its
    return pk, sig, get_certification(currency, pk, _to, _uid, _its, _isig, _cts)

def get_signed_revocation(raw_entry, currency, node_uri):
    pk, sig = [x for x in raw_entry.split(':')]
    response = requests.get(node_uri + '/wot/lookup/%s' % pk)
    json = response.json()['results']
    _uid = json[0]['uids'][0]['uid']
    _its = json[0]['uids'][0]['meta']['timestamp']
    _isig = json[0]['uids'][0]['self']
    return pk, sig, get_revocation(currency, pk, _uid, _its, _isig)

def get_signed_transaction(raw_entry, currency):
    lines = raw_entry.split('\n')
    _ver, _niss, _ninp, _nunl, _nout, _hascmt, _lt = [
        int(x) for x in lines[0][3:].split(':')]
    i = 1
    _buid, i = lines[i], i + 1
    issuers, i = lines[i:i+_niss], i + _niss
    inputs, i = lines[i:i+_ninp], i + _ninp
    unlocks, i = lines[i:i+_nunl], i + _nunl
    outputs, i = lines[i:i+_nout], i + _nout
    if _hascmt: comment, i = lines[i], i + 1 
    else: comment = ''
    signatures = lines[i:]
    assert len(issuers) == len(signatures)
    return issuers, signatures, get_transaction(
        currency, _buid, _lt, issuers, inputs, unlocks, outputs, comment)


if __name__ == '__main__':

    class InputError(Exception): pass

    entry_types = ['identity', 'joiner', 'active', 'leaver', 
        'certification', 'revoked', 'transaction']
    needs_node_uri = ['certification', 'revoked']

    print('\nEnter the block-entry type (%s), then hit <enter>' % ','.join(
        x for x in entry_types))
    entry_type = input()
    if entry_type not in entry_types: 
        raise InputError('NOT one of (%s)' % ','.join(x for x in entry_types))

    currency = os.environ.get('DUNITER_CURRENCY', 'g1-test')

    node_uri = os.environ.get('DUNITER_BMA', None)
    default = 'http://127.0.0.1:20900'
    if entry_type in needs_node_uri and not node_uri:
        print('\nEnter a duniter-node URI (default: %s), then hit <enter>' 
            % default)
        node_uri = input()
        if not node_uri: node_uri = default

    if entry_type != 'transaction':
        print('\nPaste the quoted compact %s block-entry, then hit <enter>' 
            % entry_type)
        block_entry = input()[1:-1]
    else:
        print('\nPaste the unquoted compact TX block-entry, then hit <Ctrl-D>')
        block_entry = sys.stdin.read()[1:-1]

    if entry_type == 'identity':
        pk, sig, doc = get_signed_identity(block_entry, currency)
    elif entry_type in ('joiner', 'active', 'leaver'):
        pk, sig, doc = get_signed_membership(block_entry, currency, entry_type)
    elif entry_type == 'certification':
        pk, sig, doc = get_signed_certification(block_entry, currency, node_uri)
    elif entry_type == 'revoked': 
        pk, sig, doc = get_signed_revocation(block_entry, currency, node_uri)
    elif entry_type == 'transaction': 
        pks, sigs, doc = get_signed_transaction(block_entry, currency)
    else: raise Exception('Unexpected program error')

    if entry_type != 'transaction':
        print('\n%s%s\n' % (doc, sig))
        try: 
            assert verify(pk, sig, doc)
        except ValueError: 
            print('Failed Signature Verification!!! for %s' % pk)
            raise
        print('Document Signature Verified for %s' % pk)
    else:
        print('\n%s%s\n' % (doc, '\n'.join([x for x in sigs])))
        for pk, sig in zip(pks, sigs):
            try: 
                assert verify(pk, sig, doc)
            except ValueError: 
                print('Failed Signature Verification!!! for %s' % pk)
                raise
            print('Document Signature Verified for %s' % pk)
