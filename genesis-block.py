#!/usr/bin/env python3
"""
This script generates a genesis block to create a test blockchain for Duniter software.
Then, it sends it to a Duniter node waiting for its genesis block.

It uses fake identities.
Writen in Python and using a single thread, it is fairly slow.
Some comments will tell where and how informations should be for a real currency launch.

This is no official documentation for the Duniter software.
Duniter is only aimed at managing the Ğ1 currency.
If you plan to create your own currency, please consider writing your own software or forking Duniter.
No support of any kind will be provided.

THIS IS INTENDED FOR EDUCATIONAL PURPOSE ONLY. NEVER USE THIS TO LAUNCH A REAL CURRENCY.

author : Matograine (matograine AT zaclys DOT net)
licence : CC-BY-SA v 4.0 : https://creativecommons.org/licenses/by-sa/4.0/
"""

###### imports ######
import re
import requests
import hashlib
import base64

from duniterpy.documents.identity import Identity
from duniterpy.documents.membership import Membership
from duniterpy.documents.certification import Certification
from duniterpy.documents.block import Block
from duniterpy.documents.block_uid import BlockUID, block_uid
from duniterpy.key.signing_key import SigningKey

###### useful funtions ######

def new_inner_hash(block) -> str:
  """
  Duniterpy's Block.computed_inner_hash() is bugged. (v0.61.0)
  Use this instead.
  """
  doc = block.raw()
  # get all the block content, except InnerHash, Nonce, Signature (2 last lines of raw block)
  inner_doc = "\n".join(doc.split("\n")[:-3]) + "\n"
  return hashlib.sha256(inner_doc.encode("ascii")).hexdigest().upper()

def sign_block(inner_hash, nonce, key) -> str:
  """
  Duniterpy's Block.sign() does not match the RFC specs. (v0.61.0)
  Use this instead.
  """
  # sign only Innerhash and Nonce lines (2 last lines of raw block)
  signed = "InnerHash: " + inner_hash + "\nNonce: " + str(nonce) + "\n"
  signing = base64.b64encode(key.signature(bytes(signed, "ascii")))
  signature = signing.decode("ascii")
  return signature

def new_pow(inner_hash, nonce, signature) -> str:
  """
  Duniterpy's Block.proof_of_work() requires to create the whole new block.
  This function is more efficient in our use case.
  """
  doc_str = "InnerHash: {inner_hash}\nNonce: {nonce}\n{signature}\n".format(
    inner_hash=inner_hash, nonce=nonce, signature=signature
  )
  return hashlib.sha256(doc_str.encode("ascii")).hexdigest().upper()

###### constants ######

### node ###
node = "192.168.1.75" # send to a working node with accessible BMA
port = 10925

### monetary parameters ###
# These parameters should be in the configuration of the node.
# see https://git.duniter.org/nodes/common/doc/blob/master/rfc/0009_Duniter_Blockchain_Protocol_V11.md#protocol-parameters
# and https://git.duniter.org/nodes/common/doc/blob/master/rfc/0009_Duniter_Blockchain_Protocol_V11.md#format-5
# and https://duniter.org/fr/blog/monnaies-propulsees/g1-test-redemarree/

currency = "attest"
c = "0.1" # 10%
dt = "129600" # 1.5 days
ud0 = "4200"
sigPeriod = "86400" # 1 day
sigStock = "50"
sigWindow = "1209600" # 2 weeks
sigValidity = "6048000" # 10 weeks
sigQty = "3"
idtyWindow = "1209600" # 2 weeks
msWindow = "1209600" # 2 weeks
xpercent = "0.5"
msValidity = "6048000" # 10 weeks
stepMax = "4"
medianTimeBlocks = "50"
avgGenTime = "240" # 4 minutes
dtDiffEval = "20"
percentRot = "0.8"
udTime0 = "1611774120"  # first UD. Change according to your launching date.
udReevalTime0 = "1611774120" # first reevaluation. Change also.
dtReeval = "604800"
powMin = 60

### member identities ###
# members should generate documents on their own hardware.
# these are fake identities for testing only.
# one of the members must sign the genesis block.

members = [
  {
    "seed": "0000000000000000000000000000000000000000000000000000000000000000",
    "pubkey": "4zvwRjXUKGfvwnParsHAS3HuSVzV5cA4McphgmoCtajS",
    "id": "zero",
  },
  {
    "seed": "0000000000000000000000000000000000000000000000000000000000000001",
    "pubkey": "6ASf5EcmmEHTgDJ4X4ZT5vT6iHVJBXPg5AN5YoTCpGWt",
    "id": "one",
  },
  {
    "seed": "0000000000000000000000000000000000000000000000000000000000000002",
    "pubkey": "8pM1DN3RiT8vbom5u1sNryaNT1nyL8CTTW3b5PwWXRBH",
    "id": "two",
  },
  {
    "seed": "0000000000000000000000000000000000000000000000000000000000000003",
    "pubkey": "HPYVwAQmskwT1qEEeRzhoomyfyupJGASQQtCXSNG8XS2",
    "id": "three",
  },
  {
    "seed": "0000000000000000000000000000000000000000000000000000000000000004",
    "pubkey": "J3qYF7YJMKCqQke47UQwiHizgFUXapHxRxKmxbkar89x",
    "id": "four",
  },
]

### proof-of-work ###
# base_nonce can be an arbitrary integer.
base_nonce = 42

### specific to block 0 ###
genesis_number = 0
previous_hash = None
special_previous_hash = "E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855" # sha256 of an empty string
special_block_uid = BlockUID(0, special_previous_hash)

monetaryMass = 0
unitbase = 0
issuersCount = 0
issuersFrame = 1
issuersFrameVar = 0
previousIssuer = None
dividend = None

###### create members documents ######
# Here we will create all documents at once. On real-use case, usere should :
# * first send identity and membership to the node
# * then certify all identities they vouch for

identities = list()
memberships = list()
certifications = list()

for member in members:
  # use member key
  key = SigningKey.from_seedhex(member["seed"])

  ### identity ###
  # see https://git.duniter.org/nodes/common/doc/blob/master/rfc/0009_Duniter_Blockchain_Protocol_V11.md#identity
  member_idty = Identity (
    version = 10,
    currency = currency,
    pubkey = member["pubkey"],
    uid = member["id"],
    ts = special_block_uid,
    signature = "",
  )
  member_idty.sign([key])

  identities.append(member_idty)

  ### membership ###
  # see https://git.duniter.org/documents/rfcs/-/blob/master/rfc/0010_Duniter_Blockchain_Protocol_V12.md#membership
  membership = Membership(
    version = 10,
    currency = currency,
    issuer = member["pubkey"],
    membership_ts = special_block_uid,
    membership_type = "IN",
    uid = member["id"],
    identity_ts = special_block_uid,
    signature = "",
  )
  membership.sign([key])

  memberships.append(membership)

### certifications ###
# certifications should be sent _after_ each member has sent its identity and membership.
# see https://git.duniter.org/documents/rfcs/-/blob/master/rfc/0010_Duniter_Blockchain_Protocol_V12.md#certification

for m, member in enumerate(members):
  # use member key
  key = SigningKey.from_seedhex(member["seed"])
  for i, identity in enumerate(identities):
    # no auto-certification
    if m == i:
      pass
    else:
      cert = Certification(
        version = 10,
        currency = currency,
        pubkey_from = member["pubkey"],
        identity = identities[i],
        timestamp = special_block_uid,
        signature = None,
      )
      cert.sign([key])

      certifications.append(cert)

###### generate genesis block ######

# signing member
key = SigningKey.from_seedhex(members[0]["seed"])

parameters = [
  c,
  dt,
  ud0,
  sigPeriod,
  sigStock,
  sigWindow,
  sigValidity,
  sigQty,
  idtyWindow,
  msWindow,
  xpercent,
  msValidity,
  stepMax,
  medianTimeBlocks,
  avgGenTime,
  dtDiffEval,
  percentRot,
  udTime0,
  udReevalTime0,
  dtReeval,
]

# create block with appropriate infos
genesis_block = Block(
  version = 10,
  currency = currency,
  number = genesis_number,
  powmin = powMin,
  time = int(udTime0),
  mediantime = int(udTime0),
  ud = None,
  unit_base = 0,
  issuer = members[0]["pubkey"],
  issuers_frame = issuersFrame,
  issuers_frame_var = issuersFrameVar,
  different_issuers_count = 0,
  prev_hash = None,
  prev_issuer = None,
  parameters = parameters,
  members_count = len(members),
  identities = identities,
  joiners = memberships,
  actives = [],
  leavers = [],
  revokations = [],
  excluded = [],
  certifications = certifications,
  transactions = [],
  # for now we let the three last fields empty. We calculate their values thereafter.
  inner_hash = None,
  nonce = None,
  signature = None,
)

# compute inner_hash
inner_hash = new_inner_hash(genesis_block)

###### compute proof of work ######

def begin_hash (diff):
  """
  computes the regex matching the given difficulty.
  see https://git.duniter.org/documents/rfcs/-/blob/master/rfc/0010_Duniter_Blockchain_Protocol_V12.md#br_g62-proof-of-work
  """
  number_zero = int(diff / 16)
  next_char = '{:x}'.format(15 - (diff % 16))
  return ("^" + "0" * number_zero + "[0-" + next_char + "].")

def test_begin_hash():
#  assert re.search (begin_hash (75) == "^0000[0-4]."
  if begin_hash (67) == "^0000[0-c].":
    print ("test_begin_hash OK")

## compute
nonce = base_nonce
re_pow = begin_hash(powMin)
print (re_pow)
pow = False
while pow == False:
  # sign block. Whole block is not necessary, only inner_hash and nonce.
  signature = sign_block (inner_hash, nonce, key)

  # compute pow
  block_pow = new_pow (inner_hash, nonce, signature)
  if re.search (re_pow , block_pow):
    print ("MATCH pow :", block_pow)
    genesis_block = Block(
      version = 10,
      currency = currency,
      number = genesis_number,
      powmin = powMin,
      time = int(udTime0), # - 600,
      mediantime = int(udTime0), # - 600,
      ud = None,
      unit_base = 0,
      issuer = members[0]["pubkey"],
      issuers_frame = issuersFrame,
      issuers_frame_var = issuersFrameVar,
      different_issuers_count = 0,
      prev_hash = None,
      prev_issuer = None,
      parameters = parameters,
      members_count = len(members),
      identities = identities,
      joiners = memberships,
      actives = [],
      leavers = [],
      revokations = [],
      excluded = [],
      certifications = certifications,
      transactions = [],
      inner_hash = inner_hash,
      nonce = nonce,
      signature = signature,
    )
    print (genesis_block.signed_raw())
    pow = True
  else:
    nonce += 1

###### send to the node ######
# see https://github.com/duniter/duniter-bma/blob/master/doc/API.md#blockchainblock
url = "http://" + node + ":" + str(port) + "/blockchain/block"

signed_block = {
  "block": genesis_block.signed_raw(),
  "signature": signature + "\n"
}

result = requests.post(url = url, data = signed_block)
print (result)

###### tests ######

#test_begin_hash()
